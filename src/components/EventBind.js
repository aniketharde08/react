import React, { Component } from 'react'

class EventBind extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            massage: 'Hello'
        }

        // this.clickHandler = this.clickHandler.bind(this)
    }

    // clickHandler() {
    //     this.setState({
    //         massage: 'GoodBye!'
    //     })
    //     console.log(this)
    // }

    clickHandler = () => {
        this.setState({
            massage: 'GoodBye!'
        })
    }
    
    render() {
        return (
            <div>
                <div><h1>{this.state.massage}</h1></div>
                {/* <button onClick= {this.clickHandler.bind(this)}>click</button> */}
                {/* <button onClick= {() => this.clickHandler()}>click</button> */}
                <button onClick= {this.clickHandler}>click</button>
            </div>
        )
    }
}

export default EventBind
