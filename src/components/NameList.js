import React from 'react'
import Person from './Person'

function NameList() {
    const names = ['Bruce', 'clark', 'Diana']
    const persons= [
        
        {   id:1,
            name: 'Aniket',
            age: 22,
            skill: 'React'
        },
        {   id:2,
            name: 'Mohit',
            age: 23,
            skill: 'Python'
        },
        {   id:3,
            name: 'Anil',
            age: 24,
            skill: 'Ruby'
        }
    ]
const nameList = names.map((name,index) => <h2 key={index}>{index} {name}</h2>)
    return (

        <div>{nameList}</div>
        // <div>
        //     <h2>{names[0]}</h2>
        //     <h2>{names[1]}</h2>
        //     <h2>{names[2]}</h2>
        // </div>
    )
}

export default NameList
