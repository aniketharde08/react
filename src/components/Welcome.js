import React , { Component } from 'react'

class Welcome extends Component{
    render(props) {
    const {name, nikname} = this.props
   // const {state1, state2} = this.state
    return (
        <div>
        <h1>Welcome {name} a.k.a  {nikname}</h1>
        {this.props.children}
        </div>
        )    
    }
}

export default Welcome;