import React from 'react'

const Greet = props => {
    const {name, nikname} =props
    return (
        <div>
            <h1>Hello {name} a.k.a {nikname}</h1> 
        </div>
    )
}

export default Greet;