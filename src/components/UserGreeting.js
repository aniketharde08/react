import React, { Component } from 'react'

export class UserGreeting extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isLoggedIn: true
        }
    }
    

    render() {

        return this.state.isLoggedIn && <div>Welcome Aniket</div>

        // return(
        //     this.state.isLoggedIn ? <div>Welcome Aniket</div> : <div>Welcome Guest</div>
        // )

        // let message
        // if(this.state.isLoggedIn) {
        //     message = <div>Welcome Aniket</div>
        // }else{
        //     message = <div>Welcome Guest</div>
        // }
        // return <div>{message}</div>

        // if (this.state.isLoggedIn){
        // return (
        //     <div>
        //         Welcome Aniket  
        //     </div>
        // )
        // }else{
        //     return (
        //         <div>
        //             Welcome Guest  
        //         </div>
        //     )
        // }

        // return(
        //     <div>
        //     <div>Welcome Aniket</div>
        //     <div>Welcome Aniket</div>
        // </div>
        // )
    }
}

export default UserGreeting
