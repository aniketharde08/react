import React from 'react';
import logo from './logo.svg';
import './App.css';
import Welcome from './components/Welcome';
import Greet from './components/Greet';
import Message from './components/Message';
import Counter from './components/Counter';
import FunctionClick from './components/FunctionClick';
import ClassClick from './components/ClassClick';
import EventBind from './components/EventBind';
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';
import NameList from './components/NameList';
import Stylesheet from './components/Stylesheet';
import Inline from './components/Inline';
import './appStyles.css'
import styles from './appStyles.module.css'
import Form from './components/Form';




// import Message from './components/Message';

function App() {
  return (
    <div className="App">
      <Form/>
     {/* <h1 className='error'>Error</h1>
     <h1 className={styles.success}>Success</h1> */}
    {/* <Inline/> */}
    {/* <Stylesheet primary={true}/> */}
    {/* <NameList/> */}
    {/* <UserGreeting/> */}
    {/* <ParentComponent></ParentComponent> */}
    {/* <EventBind/> */}
    {/* <FunctionClick/>
    <ClassClick/> */}
    {/* <Counter/>
    <Message />
    <Greet name= "Aniket" nikname= "Ani"/>
    <Welcome name= "Aniket" nikname ="Ani"/>
    <Welcome name= "Yadesh" nikname ="Yadu">     <button>Thank You</button>
    </Welcome>
       */}
    </div>
  );
}

export default App;
